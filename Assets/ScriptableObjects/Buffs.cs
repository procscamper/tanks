﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Buff", menuName = "ScriptableObjects/Buff",order =1)]
public class Buffs : ScriptableObject
{
    public int damageMod;
    public float reloadMod;
    public int healthMod;
    public int duration;
    public string displayText;
    public bool isBuff;
}
