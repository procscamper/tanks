﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SpawnPos", menuName = "ScriptableObjects/SpawnPos", order = 2)]
public class SpawnPositions : ScriptableObject
{
    public int mapId;
    public float xPos;
    public float yPos;
    public float zPos;
}
