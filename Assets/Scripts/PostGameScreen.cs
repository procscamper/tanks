﻿using UnityEngine;
using TMPro;
public class PostGameScreen : MonoBehaviour {
    public static PostGameScreen instance;   
    public GameObject screen;
    public TextMeshProUGUI postscreenTMP;

    private void Awake() {
        instance = this;
    }

    public void ShowScreen(string text) {
        screen.SetActive(true);
        postscreenTMP.SetText(text);
    }           
}
