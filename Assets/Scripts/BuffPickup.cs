﻿using UnityEngine;

public class BuffPickup : MonoBehaviour {
    private void OnCollisionEnter(Collision other) {
        if (other.gameObject.tag == "Player"){
            CharacterReferencer characterReference = other.collider.gameObject.GetComponent<CharacterReferencer>();
            characterReference.character.ApplyBuff();
            Destroy(gameObject);
        }
    }

}
