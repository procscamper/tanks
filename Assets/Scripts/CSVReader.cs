﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

public static class CSVReader {
    public static List<string[]> ReadCSV(string path) {
        List<string[]> CSVStringList = new List<string[]>();

            StreamReader streamReader = new StreamReader(path);
            bool hasNext = true;
            string firstLine = streamReader.ReadLine();
            while (hasNext)
            {
                string data = streamReader.ReadLine();
                if (data == null)
                {
                    hasNext = false;
                    break;
                }
                CSVStringList.Add(data.Split(','));
        }
        return CSVStringList;
    }
}


