﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Options : MonoBehaviour {
    public Slider volumeSlider;
    public Toggle invertedAxisXToggle;
    void Start() {
        bool isInverted;
        if (PlayerPrefs.GetInt("IsInverted", 0) == 0) {
            isInverted = false;
        }
        else {
            isInverted = true;
        }
        invertedAxisXToggle.isOn = isInverted;

        float soundVolume = PlayerPrefs.GetFloat("Sound",0);
        volumeSlider.value = soundVolume;
    }

    public void Save() {
        PlayerPrefs.SetFloat("Sound", volumeSlider.value);
        GameObject.FindGameObjectWithTag("Music").GetComponent<Music>().ChangeVolume(volumeSlider.value);
        if (invertedAxisXToggle.isOn) { PlayerPrefs.SetInt("IsInverted", 1); }
        else {
            PlayerPrefs.SetInt("IsInverted", 0);
        }
        PlayerPrefs.Save();
    }

    public void LoadMainMenu() {
        SceneManager.LoadScene(Constans.SceneMenu);
    }
    
}
