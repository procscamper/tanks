﻿using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour{
    public GameObject enemyPrefab;
    public List<SpawnPositions> spawnPositions;

    void Start(){
        SpawnEnemies();
    }

    private void SpawnEnemies() {
        Quaternion defaultQ = new Quaternion(0, 0, 0, 0);
        for (int i = 0; i < Difficulty.enemiesNumber; i++){
            SpawnPositions spawn = spawnPositions[i];
            GameObject enemy = Instantiate(enemyPrefab, new Vector3(spawn.xPos, spawn.yPos, spawn.zPos), defaultQ );
        }
        AIController.enemiesNumber = Difficulty.enemiesNumber;
    }
}
