﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class LevelOptions : MonoBehaviour {
    public Button mapSelectionL;
    public Button mapSelectionR;
    public Button difficultySelectionL;
    public Button difficultySelectionR;
    public Button enemiesNumberSelectionL;
    public Button enemiesNumberSelectionR;
    public TextMeshProUGUI levelNameTMP;
    public TextMeshProUGUI difficultyNameTMP;
    public TextMeshProUGUI enemiesNumberTMP;

    void Start() {
        if (Difficulty.enemiesNumber == 0) {
            Difficulty.enemiesNumber = 1;
        }
        SetupSelectors(mapSelectionL, mapSelectionR, Difficulty.levelId, 0, Constans.mapNames.Length);
        SetupSelectors(difficultySelectionL, difficultySelectionR, Difficulty.enemiesDifficulty, 0, Constans.difficulties.Length);
        SetupSelectors(enemiesNumberSelectionL, enemiesNumberSelectionR,Difficulty.enemiesNumber, 1, Constans.maxEnemies);
        levelNameTMP.SetText(Constans.mapNames[Difficulty.levelId]);
        difficultyNameTMP.SetText(Constans.difficulties[Difficulty.enemiesDifficulty]);
        enemiesNumberTMP.SetText(Difficulty.enemiesNumber.ToString());
    }

    void SetupSelectors(Button selectorL, Button selectorR, int current, int greaterThan, int smallerThan) {
        if (current - 1 >= greaterThan) {
            selectorL.gameObject.SetActive(true);

        }
        else{
            selectorL.gameObject.SetActive(false);

        }
        if (current + 1 < smallerThan) {
            selectorR.gameObject.SetActive(true);
        }
        else{
            selectorR.gameObject.SetActive(false);
        }
    }

    public void OnClickMapSelectorLeft() {
        Difficulty.levelId--;
        SetupSelectors(mapSelectionL, mapSelectionR, Difficulty.levelId, 0, Constans.mapNames.Length);
        levelNameTMP.SetText(Constans.mapNames[Difficulty.levelId]);
    }
    public void OnClickMapSelectorRight() {
        Difficulty.levelId++;
        SetupSelectors(mapSelectionL, mapSelectionR, Difficulty.levelId, 0, Constans.mapNames.Length);
        levelNameTMP.SetText(Constans.mapNames[Difficulty.levelId]);
    }
    public void OnClickDifficultySelectorLeft() {
        Difficulty.enemiesDifficulty--;
        SetupSelectors(difficultySelectionL, difficultySelectionR, Difficulty.enemiesDifficulty, 0, Constans.difficulties.Length);
        difficultyNameTMP.SetText(Constans.difficulties[Difficulty.enemiesDifficulty]);
    }
    public void OnClickDifficultySelectorRight() {
        Difficulty.enemiesDifficulty++;
        SetupSelectors(difficultySelectionL, difficultySelectionR, Difficulty.enemiesDifficulty, 0, Constans.difficulties.Length);
        difficultyNameTMP.SetText(Constans.difficulties[Difficulty.enemiesDifficulty]);
    }

    public void OnClickEnemiesNumberSelectorLeft() {
        Difficulty.enemiesNumber--;
        SetupSelectors(enemiesNumberSelectionL, enemiesNumberSelectionR, Difficulty.enemiesNumber, 1, Constans.maxEnemies);
        enemiesNumberTMP.SetText(Difficulty.enemiesNumber.ToString());
    }

    public void OnClickEnemiesNumberSelectorRigth() {
        Difficulty.enemiesNumber++;
        SetupSelectors(enemiesNumberSelectionL, enemiesNumberSelectionR, Difficulty.enemiesNumber, 1, Constans.maxEnemies);
        enemiesNumberTMP.SetText(Difficulty.enemiesNumber.ToString());
    }

    public void StartGame() {
        GameObject.FindGameObjectWithTag("Music").GetComponent<Music>().Stop();
        if (Difficulty.levelId == 0) {
            SceneManager.LoadScene(Constans.SceneMap1);
        } else if (Difficulty.levelId == 1){
            SceneManager.LoadScene(Constans.SceneMap2);
        }
    }
    public void Back() {
        SceneManager.LoadScene(Constans.SceneMenu);
    }
}
