﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
public class Shooting : MonoBehaviour
{
    public GameObject tankTower;
    public GameObject shootingTransform;
    public GameObject projectile;
    private float nextShot = 0f;
    private bool isReloading = false;
    public Character character;

    public Slider reloadSlider;
    public TextMeshProUGUI sliderTextTMP;
    public AudioSource shootingSound;

    void Update() {
        if (character.isInitialized)
        {
            if (!character.isAI)
            {
                if (Input.GetKeyDown(KeyCode.Mouse0) && Time.time >= nextShot && isReloading == false && PauseMenu.isGamePaused == false && Time.timeScale != 0)
                {
                    reloadSlider.value = 0f;
                    Shoot();
                    sliderTextTMP.SetText("Reloading...");
                    StartCoroutine(ReloadC());
                }
                if (isReloading == true)
                {
                    float barProgress = Mathf.Clamp01(Time.deltaTime / character.characterStats.reloadTime);
                    reloadSlider.value += barProgress;
                }
            }
            else
            {
                tankTower.transform.LookAt(PlayerManager.instance.player.transform);
                if (Time.time >= nextShot && isReloading == false && PauseMenu.isGamePaused == false)
                {
                    Shoot();
                    StartCoroutine(ReloadC());
                }
            }
        }
    }

    void Shoot() {
        isReloading = true;
        nextShot = Time.time + character.characterStats.reloadTime;
        GameObject bullet = Instantiate(projectile, shootingTransform.transform.position, Quaternion.identity) as GameObject;
        bullet.GetComponent<Bullet>().damage = character.characterStats.damagePerShot;
        bullet.GetComponent<Rigidbody>().AddForce(transform.forward * 0.0001f);
        shootingSound.volume = PlayerPrefs.GetFloat("Sound");
        shootingSound.Play();
    }


    IEnumerator ReloadC() {
        yield return new WaitForSeconds(character.characterStats.reloadTime);
        isReloading = false;
        if (!character.isAI){
            sliderTextTMP.SetText("Ready!");
        }
    }
}
