﻿using UnityEngine;
using UnityEngine.SceneManagement;
public class MainMenu : MonoBehaviour {
    private void Start() {
        GameObject.FindGameObjectWithTag("Music").GetComponent<Music>().Play();
    }
    public void NewGame() {
        SceneManager.LoadScene(Constans.SceneGameOptions);
    }

    public void Options() {
        SceneManager.LoadScene(Constans.SceneOptions);
    }

    public void Quit() {
        Application.Quit();
    }
}
