﻿using UnityEngine;
using System.Collections;
public class BulletCollision : MonoBehaviour {
    public AudioSource bulletHitSound;
    public Rigidbody rigidBody;
    bool hasDealtDamage = false;
    private void OnCollisionEnter(Collision other) {
        if (!bulletHitSound.isPlaying){
            bulletHitSound.volume = PlayerPrefs.GetFloat("Sound");
            bulletHitSound.Play();
        }
        rigidBody.useGravity = true;
        if (other.gameObject.tag == "Environment") {
            StartCoroutine(DestoryC());
        }
        if (!hasDealtDamage && (other.gameObject.tag == "Enemy" || other.gameObject.tag == "Player")) {
            CharacterReferencer characterReference = other.collider.gameObject.GetComponent<CharacterReferencer>();
            int damage = gameObject.GetComponent<Bullet>().damage;
            characterReference.character.ApplyDamage(damage);
            hasDealtDamage = true;
            StartCoroutine(DestoryC());
        }
    }
    IEnumerator DestoryC() {
        yield return new WaitForSeconds(1.5f);
        Destroy(gameObject);
    }
}
