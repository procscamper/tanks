﻿using UnityEngine;
using UnityEngine.AI;

public class AIController : MonoBehaviour {
    public static int enemiesNumber;
    Transform player;
    NavMeshAgent agent;
    void Start() {
        agent = GetComponent<NavMeshAgent>();
    }
    void Update() {
        player = PlayerManager.instance.player.transform;
        float distance = Vector3.Distance(player.position, transform.position);
        agent.SetDestination(player.position);
    }
}
