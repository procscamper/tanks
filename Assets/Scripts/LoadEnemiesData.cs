﻿using System.Collections.Generic;
using UnityEngine;

public class LoadEnemiesData : MonoBehaviour {   
    void Start() {
        List<string[]> enemyData = CSVReader.ReadCSV(Application.dataPath + "/Resources/CSV/EnemiesCSV.CSV");

        List<CharacterStats> enemies = new List<CharacterStats>();
        Constans.difficulties = new string[enemyData.Count];
        for (int i = 0; i < enemyData.Count; i++) {
            Constans.difficulties[i] = enemyData[i].GetValue(0).ToString();
        }
        Constans.enemyProfiles = new List<CharacterStats>();
        foreach (string[] enemy in enemyData) {
            CharacterStats c = new CharacterStats();
            c.difficultyName = enemy.GetValue(0).ToString();
            c.hp = int.Parse(enemy.GetValue(1).ToString());
            c.reloadTime = float.Parse(enemy.GetValue(2).ToString());
            c.damagePerShot = int.Parse(enemy.GetValue(3).ToString());
            Constans.enemyProfiles.Add(c);
        }
    }
}
