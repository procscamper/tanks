﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {
    public static bool isGamePaused = false;
    public GameObject pauseMenu;

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            if (isGamePaused == true) {
                ResumeGameplay();
            }
            else {
                PauseGamePlay();
            }
        }
    }

    public void ResumeGameplay() {
        isGamePaused = false;
        pauseMenu.SetActive(false);
        Time.timeScale = 1;
    }

    private void PauseGamePlay() {
        isGamePaused = true;
        pauseMenu.SetActive(true);
        Time.timeScale = 0;
    }

    public void QuitGame() {
        Application.Quit();
    }

    public void GoToMainMenu() {
        Time.timeScale = 1;
        isGamePaused = false;
        SceneManager.LoadScene(Constans.SceneMenu);
    }
}
