﻿using UnityEngine;

public class CharacterMovement : MonoBehaviour {
    public GameObject tankHull;
    public GameObject tankTower;
    float tankSpeed = 7f;
    float tankRotationSpeed = 50f;
    float towerRotationSpeed = 40f;
    int inversionX = 1;
    private void Start() {
        if (PlayerPrefs.GetInt("IsInverted", 0) == 0)
        {
            inversionX = 1;
        }
        else {
            inversionX = -1;
        }
    }
    void Update() {
        HullMovement();
        TowerMovement();
    }

    void HullMovement() {
        Vector3 position = tankHull.transform.position;
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A)) {
            tankHull.transform.Rotate(0, -Time.deltaTime * tankRotationSpeed, 0);
        }
        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D)) {
            tankHull.transform.Rotate(0, Time.deltaTime * tankRotationSpeed, 0);
        }
        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W)) {
            position += Time.deltaTime * tankHull.transform.forward * tankSpeed;
        }
        if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S)) {
            position -= Time.deltaTime * tankHull.transform.forward * tankSpeed;
        }
        tankHull.transform.position = position;
    }

    void TowerMovement() {
        if (inversionX * Input.GetAxis("Mouse X") < 0) {
            tankTower.transform.Rotate(0, -Time.deltaTime * towerRotationSpeed, 0);
        }
        if (inversionX * Input.GetAxis("Mouse X") > 0) {
            tankTower.transform.Rotate(0, Time.deltaTime * towerRotationSpeed, 0);
        }
    }
}
