﻿using UnityEngine;
public class Music: MonoBehaviour
{
    public AudioSource audioSource;
    private void Awake() {
        DontDestroyOnLoad(gameObject);
        audioSource = GetComponent<AudioSource>();
    }

    public void Play() {
        if (!audioSource.isPlaying) {
            audioSource.volume = PlayerPrefs.GetFloat("Sound");
            audioSource.Play();
        }
    }
    public void ChangeVolume(float value) {
        audioSource.volume = value;
    }
    public void Stop() {
        audioSource.Stop();
    }
}
