﻿using System.Collections.Generic;

public static class Constans
{
    // Default values for player
    public static int hp = 100;
    public static float reloadTime = 1f;
    public static int damagePerShot = 25;
    //
    public static string [] mapNames = { "Garden", "Desert" };
    public static string SceneMenu = "Menu";
    public static string SceneOptions = "Menu-Options";
    public static string SceneGameOptions = "Menu-GameOptions";
    public static string SceneMap1 = "Map1-Garden";
    public static string SceneMap2 = "Map2-Desert";

    public static int maxEnemies = 4;
    public static string[] difficulties;
    public static List<CharacterStats> enemyProfiles;
}
