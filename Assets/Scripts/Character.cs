﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Character : MonoBehaviour {
    public CharacterStats characterStats;
    public TextMeshProUGUI buffDisplay;
    public List<Buffs> buffList;
    public Slider hpBar;
    public bool isAI = false;
    public bool isInitialized = false;
    void Start(){
        InitializeTank();
    }

    public void InitializeTank() {
        if (tag == "Player"){
            characterStats = new CharacterStats();
            characterStats.hp = Constans.hp;
            characterStats.reloadTime = Constans.reloadTime;
            characterStats.damagePerShot = Constans.damagePerShot;
            isInitialized = true;
        }
        if (tag == "Enemy") {
            Character character = gameObject.GetComponent<Character>();
            CharacterStats characterStats = new CharacterStats();
            characterStats.hp = Constans.enemyProfiles[Difficulty.enemiesDifficulty].hp;
            characterStats.damagePerShot = Constans.enemyProfiles[Difficulty.enemiesDifficulty].damagePerShot;
            characterStats.reloadTime = Constans.enemyProfiles[Difficulty.enemiesDifficulty].reloadTime;
            character.characterStats = characterStats;
            isAI = true;
            isInitialized = true;
        }
    }

    public void ApplyDamage(int damage){
        characterStats.hp -= damage;
        if (gameObject.tag == "Player") {
            hpBar.value = characterStats.hp;
        }
        if (characterStats.hp <= 0) {
            if (gameObject.tag == "Enemy") {
                Destroy(gameObject);
                AIController.enemiesNumber--;
                if (AIController.enemiesNumber == 0) {
                    PostGameScreen.instance.ShowScreen("Victory!");
                    Time.timeScale = 0f;
                }
            }
            else {
                PostGameScreen.instance.ShowScreen("You died");
                Time.timeScale = 0f;
            }
        }
    }

    public void ApplyBuff() {
        StartCoroutine(BuffC());
    }

    IEnumerator BuffC() {
        int randomResult = UnityEngine.Random.Range(0, 6);
        Buffs buff = buffList[randomResult];
        buffDisplay.enabled = true;
        buffDisplay.SetText(buff.displayText);
        if (buff.isBuff) {
            buffDisplay.color = Color.green;
        } 
        else {
            buffDisplay.color = Color.red;
        }
        characterStats.hp += buff.healthMod;
        if (characterStats.hp > 100) { characterStats.hp = 100;}
        hpBar.value = characterStats.hp;
        characterStats.damagePerShot += buff.damageMod;
        characterStats.reloadTime += buff.reloadMod;
        yield return new WaitForSeconds(buff.duration);
        buffDisplay.enabled = false;
        characterStats.hp -= buff.healthMod;
        characterStats.damagePerShot -= buff.damageMod;
        characterStats.reloadTime -= buff.reloadMod;
    }
}
