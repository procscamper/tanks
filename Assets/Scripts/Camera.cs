﻿using UnityEngine;

public class Camera : MonoBehaviour {
    public GameObject tankTower;
    public GameObject cameraFocus;
    Vector3 velocity;
    Vector3 defaultDistance = new Vector3(0,2.5f,-5);
    float distanceDamp = 0.12f;
    float rotationSpeed = 0.1f;

    void LateUpdate() {
        SmoothFollow();
        SmoothRotate();
    }

    void SmoothFollow() {
        Vector3 destinationPos = tankTower.transform.position + (tankTower.transform.rotation * defaultDistance);
        Vector3 currentPos = Vector3.SmoothDamp(transform.position, destinationPos, ref velocity, distanceDamp);
        transform.position = currentPos;
        transform.LookAt(cameraFocus.transform, tankTower.transform.up);
    }

    void SmoothRotate() {
        transform.rotation = Quaternion.Lerp(transform.rotation, tankTower.transform.rotation, rotationSpeed * Time.deltaTime);
    }
}
